import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';
import Login from './screens/Login';
import Register from './screens/Register';
import Task from './screens/Task'
import Main from './screens/Main'
import {createStackNavigator, createAppContainer} from 'react-navigation';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      currentScreen: 'Splash'
    }
    //Check if user already login
    setTimeout(() => {
      this.setState({currentScreen: 'Login'})
    }, 5000);
  }
  render() {
    let Splashscreen = <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Image style={styles.logo}
                source={require('./images/logo.png')}>
            </Image>
          </View>
    </View>
    let screenToDisplay = this.state.currentScreen == 'Splash' ? Splashscreen : <Login/>
    return (
      screenToDisplay
    );
  }
}

const AppStackNavigator = createStackNavigator({
  Login: {screen: Login},
  Register: {screen: Register},
  Task: {screen: Task},
  Main: {screen: Main}
  
});

const AppContainer = createAppContainer(AppStackNavigator);

export default AppContainer

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'column'
  },
  logoContainer:{
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1
  },
  logo:{
      width: 128,
      height:62
  }
});
