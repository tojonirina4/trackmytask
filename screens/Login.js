import React, {Component} from 'react';
import { StyleSheet, Text, View,
Image, TouchableWithoutFeedback, StatusBar, TextInput, SafeAreaView,
Keyboard, TouchableOpacity, KeyboardAvoidingView, Alert, ActivityIndicator, AsyncStorage} from 'react-native';
import Store from 'react-native-store';
import { withNavigation } from 'react-navigation';
var navigation;
const uuidv1 = require('uuid/v1');
const ACCESS_TOKEN = "ACCESS_TOKEN";
const DB = {
    'user': Store.model('user')
}


 class Login extends Component{
    static navigationOptions = {
        header: null
    };
    constructor(props){
        super(props);
        navigation = this.props.navigation;
        this.state = {
            userNameTextInput: '',
            passwordTextInput: '' ,
            isLoading: false,
            splashScreen: true
        }
    }
    componentDidMount(){
        this._checkSession();
    }

    _checkSession = async () => {
        const value = await AsyncStorage.getItem(ACCESS_TOKEN);
        this.setState({splashScreen: false});
        if(value){
            navigation.navigate('Main', {currentUserId: value.userId});
        }
    }

    validate = () => {
        if(this.state.userNameTextInput == '' || this.state.passwordTextInput == ''){
            Alert.alert('Validation Error', 'username/email and password are mandatory!');
            return;
        }
        this.setState(isLoading => ({isLoading: true}));
        DB.user.find({
            where: {
                and:[{username: this.state.userNameTextInput.toLowerCase()}, {password: this.state.passwordTextInput}]
            }
        }).then((value)=> {
            if(!value || value.lenght == 0){
                Alert.alert('Error', 'Login Failed!');
                this.setState(isLoading => ({isLoading: false}));
                return;
            }
            Alert.alert(
                'Success',
                'Login Success. Please note that pull is required to refresh your data. Enjoy!',
                [
                  {text: 'Ok', onPress: () => {
                    this._proceed(value[0]);
                  }}
                ]
            );
        })
    }

    async _proceed(user){
        console.log("user tokomy " + JSON.stringify(user))
        try {
            let uni = uuidv1();
            let session = {
                token: uni,
                userId: user._id
            }
            await AsyncStorage.setItem(ACCESS_TOKEN, JSON.stringify(session));
            this.setState(isLoading => ({isLoading: false}));
            navigation.navigate('Main', {});
        } catch (error) {
            console.log(error);
            Alert.alert('Error', 'Error when creating Sesssion', [
                {text: 'Ok', onPress: () => {
                    navigation.navigate('Login', {});
                }}
            ]);
        }
    }

    render() {
        const Splashscreen = <View style={styles.container}>
    <View style={styles.logoContainer}>
      <Image style={styles.logo}
          source={require('../images/logo.png')}>
      </Image>
    </View>
  </View>;
  const login = <SafeAreaView style={styles.container}>
          <KeyboardAvoidingView behavior="padding" style={styles.container}>
              <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                  <View style={styles.container}>
                      <View style={styles.logoContainer}>
                          <Image style={styles.logo}
                              source={require('../images/logo.png')}>
                          </Image>
                      </View>
                      <View style={styles.infoContainer}>
                          <TextInput style={styles.input}
                              placeholder="Enter username or email"
                              placeholderTextColor="#ffffff"
                              keyboardType="email-address"
                              returnKeyType="next"
                              autoCorrect={false}
                              onChangeText={userNameTextInput => this.setState({userNameTextInput})}
                              onSubmitEditing={() => this.refs.txtPassword.focus()}
                              >
                          </TextInput>
                          <TextInput style={styles.input}
                              placeholder="Enter password"
                              placeholderTextColor="#ffffff"
                              secureTextEntry
                              returnKeyType="go"
                              onChangeText={passwordTextInput => this.setState({passwordTextInput})}
                              autoCorrect={false}
                              ref={"txtPassword"}
                              >
                          </TextInput>
                          <TouchableOpacity style={styles.buttonContainer}
                          onPress={() => this.validate()}>
                              <Text style={styles.buttonText}>SIGN IN</Text>
                          </TouchableOpacity>
                          <Text style={styles.link}  onPress={() => navigate('Register', {})}>Don't have an account? Sign Up here</Text>
                      </View>
                  </View>
              </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
      </SafeAreaView>;
        const {navigate} = this.props.navigation;
        if(this.state.isLoading){
            return(
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color="#0000ff"/>
                <Text style={styles.buttonText}>loading</Text>
              </View>
            )
        }
        return (
            this.state.splashScreen  ? Splashscreen : login
        );
    }
}

const styles = StyleSheet.create({
    activityIndicatorContainer:{
        flex: 1,
        justifyContent: 'center'
    },
    container: {
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'column'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logo:{
        width: 128,
        height:62
    },
    infoContainer:{
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 250,
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: '#747578',
        color: 'white',
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 20
    },
    buttonContainer:{
        backgroundColor: '#f77f23',
        paddingVertical: 15,
        borderRadius: 10,
        marginBottom: 10
    },
    buttonText:{
        textAlign: 'center',
        color: '#46474a',
        fontWeight: 'bold',
        fontSize: 18
    },
    link:{
        fontStyle: "italic",
        textAlign: 'center',
        color: '#46474a',
        textDecorationLine: "underline"
    }
  });

  export default withNavigation(Login);
  