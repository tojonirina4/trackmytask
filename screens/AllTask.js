import React, {Component} from 'react';
import { StyleSheet, Text, View, Alert} from 'react-native';
import {List, ListItem } from 'react-native-elements';
import Store from 'react-native-store';
import {FlatList} from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Task from './Task';
import Swipeout from 'react-native-swipeout';
var currentUserId = 0;
const DB = {
    'task': Store.model('task')
}

class AllTask extends Component{
    static navigationOptions = {
        header: null
    };
    componentDidMount(){
        this._fetchData();
    }

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            page: 1,
            seed: 1,
            error: null,
            refreshing: false,
            activeRowKey: null
        }
        currentUserId = this.props.screenProps.userId;
    }

    _displayIcon(item){
        let iconName = item.completed ? 'md-checkmark-circle' : 'md-arrow-dropright-circle';
        let colorStyle = item.completed ? 'completedCheck' : 'toDoCheck';
        return <Icon 
        onPress={() => _logout()}
        style={styles[colorStyle]} name={iconName} size={30}/>
    }

    _handleRefresh = () => {
        this.setState({
            page: 1,
            refreshing: true,
            seed: this.state.seed + 1,
        }, () => {
            this._fetchData();
        })
    }

    render(){
        const {navigate} = this.props.navigation;
        return (
                <FlatList
                    data={this.state.data}
                    renderItem={({ item, index }) => (
                        <Swipeout
                            right={
                                [
                                    {
                                        onPress: () => {
                                        Alert.alert(
                                            'Alert',
                                            'Are you sure you want to delete ?',
                                            [
                                            {text: 'No', onPress: () => console.log('Canceled')},
                                            {text: 'Yes', onPress: () => {
                                                    this.state.data.splice(index, 1);
                                                    DB.task.removeById(item._id).then((res) => {
                                                        this._handleRefresh();
                                                    })
                                                }},
                                            ],
                                            {cancelable: true},
                                       );
                                    },
                                    text: 'Delete', type: 'delete'
                                }
                            ]}
                            left={
                                [
                                    {
                                        onPress: () => {
                                        if(item.completed){
                                            return;
                                        }
                                        Alert.alert(
                                            'Info',
                                            'The task will be mark as completed! Do you confirm?',
                                            [
                                            {text: 'No', onPress: () => console.log('Canceled')},
                                            {text: 'Yes', onPress: () => {
                                                    let task = {
                                                        title: item.title,
                                                        startDate: item.startDate,
                                                        endDate: item.endDate,
                                                        description: item.description,
                                                        completed: true
                                                    }
                                                    DB.task.updateById(task, item._id).then((res) => {
                                                        this._handleRefresh();
                                                    })
                                                }},
                                            ],
                                            {cancelable: true},
                                       );
                                    },
                                    text: item.completed ? 'Completed' : 'Set as Completed', type: item.completed ? 'default' : 'secondary'
                                }
                            ]}
                            autoClose={true}>
                            <ListItem
                            onPress={() =>  navigate('Task', {item})}
                            title={item.title}
                            subtitle= {'Due to: ' + item.endDate + ' // ' + item.description}
                            rightElement={this._displayIcon(item)}
                        />
                        </Swipeout>
                    )}
                    keyExtractor={item => item._id.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={this._handleRefresh}
                />
        );
    }

    _goToDetails = (itemId) => {

        Alert.alert('Item Pressed', 'ItemId selected ' + itemId);
    }

    _fetchData = async () =>{
        DB.task.find({
            where: {userId: currentUserId}
        }).then((resp)=> {
            this.setState({data: resp, refreshing: false});
            console.log('indro ary n res ' + JSON.stringify(this.state.data));
        });
    }
}

const AppStackNavigator = createStackNavigator({
    All: {screen: AllTask},
    Task: {screen: Task}
});

const AppContainer = createAppContainer(AppStackNavigator);

export default AppContainer

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'column'
    },
    completedCheck:{
        paddingRight: 10,
        color: 'green'
    }, 
    toDoCheck:{
        paddingRight: 10,
        color: 'red'
    }
})