import React, {Component} from 'react';
import { StyleSheet, Text, View,
Image, TouchableWithoutFeedback, StatusBar, TextInput, SafeAreaView,
Keyboard, TouchableOpacity, KeyboardAvoidingView, Alert, ActivityIndicator, AsyncStorage} from 'react-native';
import Store from 'react-native-store';
const DB = {
    'user': Store.model('user')
}
const uuidv1 = require('uuid/v1');
var navigation;

export default class Register extends Component{
    static navigationOptions = {
        title: 'New User'
    };
    constructor(props){
        super(props);
        navigation = this.props.navigation;
        this.state = {
            userNameTextInput: '',
            passwordTextInput: '' ,
            confrimPasswordTextInput: '' ,
            isLoading: false
        }
    };
    
    validate = async () => {
        if(this.state.userNameTextInput == '' || this.state.passwordTextInput == '' || this.state.confrimPasswordTextInput == ''){
            Alert.alert('Validation Error', 'username/email and password are mandatory!');
            return;
        }
        this.setState(isLoading => ({isLoading: true}));
        try {
            DB.user.find({
                where: {
                    username: this.state.userNameTextInput.toLowerCase()
                }
            }).then((value)=> {
                if (value) {
                    this.handleError('Username Already exists');
                    return;
                }
                if(this.state.passwordTextInput != this.state.confrimPasswordTextInput){
                    this.handleError('Password doesn\'t match');
                    return;
                }
                let user = {
                    username: this.state.userNameTextInput.toLowerCase(),
                    password: this.state.passwordTextInput
                }
                DB.user.add(user).then(createUser => this._proceed(createUser), error => {
                    Alert.alert('Error', 'Error when creating user');
                } );
            });
          } catch (error) {
            // Error retrieving data
            this.handleError('An Error has occured when validating data!');
            return;
          } 
    }

    async _proceed(user){
        console.log('iot le vao cree ' + user);
        try {
            let uni = uuidv1();
            let session = {
                token: uni,
                userId: user._id
            }
            await AsyncStorage.setItem('ACCESS_TOKEN', JSON.stringify(session));
            Alert.alert(
                'Success',
                'User Created with success. Let\'s created your first task!',
            [
                {text: 'OK', onPress: () => {
                    navigation.navigate('Task', {});
                    this.setState(isLoading => ({isLoading: false}));
                }},
            ],
            {cancelable: false},
          );
        } catch (error) {
            console.log(error);
            Alert.alert('Error', 'Error when creating Sesssion', [
                {text: 'Ok', onPress: () => {
                    navigation.navigate('Login', {});
                }}
            ]);
        }
    }

    handleError =  (errorTest) => {
        this.setState(isLoading => ({isLoading: false}));
        Alert.alert('Error', errorTest);
    }

    render() {
        if(this.state.isLoading){
            return(
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color="#0000ff"/>
                <Text style={styles.buttonText}>loading</Text>
              </View>
            )
        }
        return (<SafeAreaView style={styles.container}>
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        <View style={styles.logoContainer}>
                            <Image style={styles.logo}
                                source={require('../images/logo.png')}>
                            </Image>
                        </View>
                        <View style={styles.infoContainer}>
                            <TextInput style={styles.input}
                                placeholder="Create your username"
                                placeholderTextColor="#ffffff"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={userNameTextInput => this.setState({userNameTextInput})}
                                onSubmitEditing={() => this.refs.txtPassword.focus()}
                                >
                            </TextInput>
                            <TextInput style={styles.input}
                                placeholder="Enter password"
                                placeholderTextColor="#ffffff"
                                secureTextEntry
                                onChangeText={passwordTextInput => this.setState({passwordTextInput})}
                                autoCorrect={false}
                                ref={"txtPassword"}
                                >
                            </TextInput>
                            <TextInput style={styles.input}
                                placeholder="Confrim password"
                                placeholderTextColor="#ffffff"
                                secureTextEntry
                                returnKeyType="go"
                                onChangeText={confrimPasswordTextInput => this.setState({confrimPasswordTextInput})}
                                autoCorrect={false}
                                ref={"txtPassword"}
                                >
                            </TextInput>
                            <TouchableOpacity style={styles.buttonContainer}
                            onPress={() =>  this.validate()}>
                                <Text style={styles.buttonText}>GO</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>);
    }
}


const styles = StyleSheet.create({
    activityIndicatorContainer:{
        flex: 1,
        justifyContent: 'center'
    },
    container: {
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'column'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logo:{
        width: 128,
        height:62
    },
    infoContainer:{
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 290,
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: '#747578',
        color: 'white',
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 20
    },
    buttonContainer:{
        backgroundColor: '#f77f23',
        paddingVertical: 15,
        borderRadius: 10,
        marginBottom: 10
    },
    buttonText:{
        textAlign: 'center',
        color: '#46474a',
        fontWeight: 'bold',
        fontSize: 18
    },
    link:{
        fontStyle: "italic",
        textAlign: 'center',
        color: '#46474a',
        textDecorationLine: "underline"
    }
  });
  