import React, {Component} from 'react';
import { StyleSheet, Text, View,
Image, TouchableWithoutFeedback, StatusBar, TextInput, SafeAreaView,
Keyboard, TouchableOpacity, KeyboardAvoidingView, Alert, ActivityIndicator, AsyncStorage} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Store from 'react-native-store';
import Icon from '@expo/vector-icons/Ionicons';
const ACCESS_TOKEN = "ACCESS_TOKEN";
var currentUserId = 0;
const DB = {
    'task': Store.model('task')
}

const uuidv1 = require('uuid/v1');
var dateFormat = require('dateformat');
const initialState = {
    title: '',
    startDate: 'Click to select a date to start' ,
    endDate: 'Click to select a deadline' ,
    description: '',
    isDateTimePickerVisible: false,
    datePickerCaller: '',
    readOnly: false,
    completed: false,
    isLoading: false
};
export default class Login extends Component{
    static navigationOptions = {
        header: null
    };
    _showDateTimePicker = () => {
        if(!this.state.readOnly){
            this.setState({ isDateTimePickerVisible: true });
        }
    }
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this.setState({[this.state.datePickerCaller]: dateFormat(date, "dd-mm-yyyy")});
        this._hideDateTimePicker();
      };
    constructor(props){
        super(props);
        this.state = initialState;
    }

    componentWillMount(){
        this.setState({isLoading: true});
    }

    componentDidMount(){
        this._setData();
    }

    _setData = async () =>{
        const value = await AsyncStorage.getItem(ACCESS_TOKEN);
        if(value){
            currentUserId =  JSON.parse(value).userId;
        }else{
            this._logout();
        }
        let item = this.props.navigation.getParam('item', undefined);
        if(item){
            this.setState({
                readOnly: true, title: item.title, startDate: item.startDate,
                    endDate: item.endDate, description: item.description,
                    completed: item.completed});
        }
        this.setState({isLoading: false});
    }

    _handleError =  (errorTest) => {
        this.setState(isLoading => ({isLoading: false}));
        Alert.alert('Error', errorTest);
    }

    _resetForm = () => {
        this.setState(initialState);
    }

    _edit = () => {
        this.setState({readOnly: false});
    }

    _validate = async (item) => {
        if(this.state.title == '' || this.state.startDate == '' || this.state.endDate == ''){
            Alert.alert('Validation Error', 'title, startDate and enDate field are mandatory!');
            return;
        }
        this.setState(isLoading => ({isLoading: true}));
        try {
            let task = {
                title: this.state.title,
                startDate: this.state.startDate,
                endDate: this.state.endDate,
                description: this.state.description,
                completed: this.state.completed,
                userId: currentUserId
            }
            if(item){
                DB.task.updateById(task, item._id);
            }else{
                DB.task.add(task);
            }
            this.setState(isLoading => ({isLoading: false}));
            Alert.alert(
                'Success',
                'Task Added! Please Refresh your list.',
                [
                  {text: 'Ok', onPress: () => {
                    this._resetForm();
                    this.props.navigation.navigate('Main', {});
                  }}
                ],
                {cancelable: false},
              );

          } catch (error) {
            // Error retrieving data
            console.error(error);
            this._handleError('An Error has occured when validating data!');
            return;
          } 
    }

    _displayFormHeader(item){
       return item ? item.completed ? <Text style={styles.title}>
                                {item.title}
                            </Text> : <Text style={styles.title}>
                                {item.title + "   " }
                                <Icon 
                                    onPress={() => this._edit()}
                                    style={styles.headerIcon} name='md-open' size={20}/>
                            </Text> : 
                    <Text style={styles.title}>
                        ABOUT YOUR TASK...
                    </Text>
    }

    _displayAddButton(item){
        if(!this.state.readOnly){
            return <TouchableOpacity style={styles.buttonContainer} onPress={() => this._validate(item)}>
                                <Text style={styles.buttonText}>
                                    {
                                        item ? "UPDATE" : "ADD TO MY LIST"
                                    }
                                </Text>
                    </TouchableOpacity>
        }
    }

    _displayCancelButton(item){
        if(!this.state.readOnly && item){
            return <TouchableOpacity style={styles.buttonContainer} onPress={() => this._cancel(item)}>
                                <Text style={styles.buttonText}>CANCEL</Text>
                    </TouchableOpacity>
        }
    }

    _cancel(){
        this.setState({readOnly: true});
        this.props.navigation.navigate('All', {})
    }

    render() {
        const { navigation } = this.props;
        let item = navigation.getParam('item', undefined);
        if(this.state.isLoading){
            return(
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color="#0000ff"/>
                <Text style={styles.buttonText}>loading</Text>
              </View>
            )
        }
        return (
            <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                        <View style={styles.infoContainer}>
                            {this._displayFormHeader(item)}
                            <Text style={styles.fieldDesc}>Title</Text>
                            <TextInput style={styles.input}
                                placeholder="Enter a title for your task"
                                placeholderTextColor="#ffffff"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={title => this.setState({title})}
                                editable={!this.state.readOnly}
                                >
                                {this.state.title}
                            </TextInput>
                            <Text style={styles.fieldDesc}>Start On</Text>
                            <Text style={styles.input}
                                placeholder=""
                                placeholderTextColor="#ffffff"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={startDate => this.setState({startDate})}
                                onPress={() => {this._showDateTimePicker(); this.setState({datePickerCaller: 'startDate'})}}
                                >
                                {this.state.startDate}
                            </Text>
                            <Text style={styles.fieldDesc}>Should be completed On</Text>
                            <Text style={styles.input}
                                placeholder=""
                                placeholderTextColor="#ffffff"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={endDate => this.setState({endDate})}
                                onPress={() => {this._showDateTimePicker(); this.setState({datePickerCaller: 'endDate'})}}
                                >
                                {this.state.endDate}
                            </Text>
                            <Text style={styles.fieldDesc}>Description</Text>
                            <TextInput style={styles.inputMultipleLine}
                                placeholder="Tell me more about it..."
                                placeholderTextColor="#ffffff"
                                returnKeyType="next"
                                autoCorrect={false}
                                multiline = {true}
                                onChangeText={description => this.setState({description})}
                                editable={!this.state.readOnly}
                                >
                                {this.state.description}
                            </TextInput>
                            {
                                this._displayAddButton(item)
                            }
                            {
                                this._displayCancelButton(item)
                            }
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    activityIndicatorContainer:{
        flex: 1,
        justifyContent: 'center'
    },
    container: {
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'column'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'red'
    },
    fieldDesc:{
        paddingLeft: 10,
        color: "grey"
    },
    logo:{
        width: 128,
        height:62
    },
    infoContainer:{
        alignItems: 'stretch',
        justifyContent: 'center',
        flex: 1,
        paddingHorizontal: 15,
    },
    input: {
        height: 40,
        backgroundColor: '#747578',
        color: 'white',
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 20,
        textAlignVertical: 'center'
    },
    title:{
        textAlign: 'center',
        fontSize: 25,
        marginBottom: 30
    },
    inputMultipleLine: {
        height: 100,
        backgroundColor: '#747578',
        color: 'white',
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 20
    },
    
    buttonContainer:{
        backgroundColor: '#f77f23',
        paddingVertical: 15,
        borderRadius: 10,
        marginBottom: 10
    },
    buttonText:{
        textAlign: 'center',
        color: '#46474a',
        fontWeight: 'bold',
        fontSize: 18
    },
    link:{
        fontStyle: "italic",
        textAlign: 'center',
        color: '#46474a',
        textDecorationLine: "underline"
    },
    headerIcon:{
        paddingRight: 10,
        color: 'grey'
    }
  });
  