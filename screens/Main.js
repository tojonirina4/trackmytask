import React, {Component} from 'react';
import { StyleSheet, Text, View, Alert, BackHandler, AsyncStorage, ActivityIndicator} from 'react-native';
import {createBottomTabNavigator, createAppContainer, createStackNavigator} from 'react-navigation';
import Icon from '@expo/vector-icons/Ionicons';
import Task from './Task';
import All from './AllTask';
import Completed from './CompletedTask';
import TodayTask from './TodayTask';
import Login from './Login';
import { updateFocus } from 'react-navigation-is-focused-hoc';
var  navigation = {};
const ACCESS_TOKEN = "ACCESS_TOKEN";
var currentUserId = 0;
export default class Main extends Component{

    constructor(props){
        super(props);
        console.log("misy v ny navigation ");
        navigation = this.props.navigation;
        this.state = {
            isLoading: true
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this._setCurrentUser();
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
    
      handleBackPress = () => {
        return true;  // Do nothing when back button is pressed
      }

    static navigationOptions = {
        header: null
    };
    render() {
        if(this.state.isLoading){
            return(
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color="#0000ff"/>
                <Text style={styles.buttonText}>loading</Text>
              </View>
            )
        }
        return(
            <AppContainer screenProps={{ userId: currentUserId}}/>
        )
    }

    _setCurrentUser = async () => {
        const value = await AsyncStorage.getItem(ACCESS_TOKEN);
        if(value){
            currentUserId =  JSON.parse(value).userId;
            this.setState({isLoading: false});
        }else{
            this._logout();
        }
    }
}

_logout = () => {
    Alert.alert(
        'Info',
        'Are you sure you want to logout?',
        [
        {text: 'No', onPress: () => console.log('Canceled')},
        {text: 'Yes', onPress: () => {
            _proceed();
        }},
        ],
        {cancelable: true},
   );
}

_proceed = async () =>{
    await AsyncStorage.removeItem(ACCESS_TOKEN);
            navigation.navigate('Login', {});
}

const MainTabNavigator = createBottomTabNavigator({
    Today: TodayTask,
    Completed,
    All,
    New: Task,
},
{
    navigationOptions:({navigation}) => {
        const {routeName} = navigation.state.routes
        [navigation.state.index];
        return {
            headerTitle: routeName
        };
    }
});



const MainStackNavigator  =  createStackNavigator({
    MainTabNavigator
},{
    defaultNavigationOptions:({navigation}) => {
        return {
            headerRight: <Icon 
                onPress={() => _logout()}
                style={styles.headerIcon} name='md-log-out' size={30}/>,
            headerStyle: {
                backgroundColor: '#f77f23'
            },
            headerTintColor: '#fff'
        }
    }
});

const AppContainer = createAppContainer(MainStackNavigator); 

const styles = StyleSheet.create({
    activityIndicatorContainer:{
        flex: 1,
        justifyContent: 'center'
    },
    container: {
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'column'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    headerIcon:{
        paddingRight: 10,
        color: '#fff'
    }, 
    logo:{
        width: 128,
        height:62
    },
    infoContainer:{
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 250,
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: '#747578',
        color: 'white',
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 20
    },
    buttonContainer:{
        backgroundColor: '#f77f23',
        paddingVertical: 15,
        borderRadius: 10,
        marginBottom: 10
    },
    buttonText:{
        textAlign: 'center',
        color: '#46474a',
        fontWeight: 'bold',
        fontSize: 18
    },
    link:{
        fontStyle: "italic",
        textAlign: 'center',
        color: '#46474a',
        textDecorationLine: "underline"
    },
    activityIndicatorContainer:{
        flex: 1,
        justifyContent: 'center'
    }
  })